# BLOG

This project is built via [echo](https://github.com/labstack/echo),[sqlboiler](https://github.com/volatiletech/sqlboiler) and SQLite3

# HOW TO RUN

- Assume the docker and docker-compose are installed
- Docker version is **19.03.8** and docker-compose version is **1.25.5**
- SQLite3 should be preinstalled on the macOS
- Run `bash start.sh` at the root of the project, it will create a sqlite database and initialize it
- Then run `docker-compose up -d` at the root of the project

# HOW TO TEST OR BUILD DEVELOPMENT ENVIRONMENT

- If `start.sh` is executed, the development environment is ready
- Run `go test ./...` or `go test -cover ./...` at the root of the project