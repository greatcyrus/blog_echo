package apisvr

import (
	"github.com/labstack/echo"
)

func RegisterArticleService(e *echo.Echo, srv ArticleService) {
	e.POST("/articles", CreateArticleHandler(srv))
	e.GET("/articles/:id", RetrieveArticleHandler(srv))
	e.GET("/articles", ListArticleHandler(srv))
}
