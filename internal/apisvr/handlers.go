package apisvr

import (
	"net/http"

	"github.com/labstack/echo"
)

type CreateArticleReq struct {
	Title   string `json:"title" validate:"required"`
	Content string `json:"content" validate:"required"`
	Author  string `json:"author" validate:"required"`
}

type CreateArticleResp struct {
	ID int `json:"id"`
}

type RetrieveArticleResp struct {
	ID      int    `json:"id"`
	Title   string `json:"title"`
	Content string `json:"content"`
	Author  string `json:"author"`
}

type RespFormat struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type ArticleService interface {
	CreateArticle(c echo.Context, req *CreateArticleReq) (resp *CreateArticleResp, err error)

	RetrieveArticle(c echo.Context) (resp *RetrieveArticleResp, err error)

	ListArticle(c echo.Context) (resp []*RetrieveArticleResp, err error)
}

func CreateArticleHandler(srv ArticleService) echo.HandlerFunc {
	return func(c echo.Context) (err error) {
		req := new(CreateArticleReq)
		if err = c.Bind(req); err != nil {
			respFormat := &RespFormat{
				Status:  http.StatusBadRequest,
				Message: err.Error(),
				Data:    nil,
			}
			return c.JSON(http.StatusBadRequest, respFormat)
		}
		resp, err := srv.CreateArticle(c, req)
		if err != nil {
			respFormat := &RespFormat{
				Status:  http.StatusInternalServerError,
				Message: err.Error(),
				Data:    resp,
			}
			return c.JSON(http.StatusInternalServerError, respFormat)
		}
		respFormat := &RespFormat{
			Status:  http.StatusCreated,
			Message: "Success",
			Data:    resp,
		}
		return c.JSON(http.StatusCreated, respFormat)
	}
}

func RetrieveArticleHandler(srv ArticleService) echo.HandlerFunc {
	return func(c echo.Context) (err error) {
		resp, err := srv.RetrieveArticle(c)
		if err != nil {
			respFormat := &RespFormat{
				Status:  http.StatusNotFound,
				Message: err.Error(),
				Data:    resp,
			}
			return c.JSON(http.StatusNotFound, respFormat)
		}
		respFormat := &RespFormat{
			Status:  http.StatusOK,
			Message: "Success",
			Data:    resp,
		}
		return c.JSON(http.StatusOK, respFormat)
	}
}

func ListArticleHandler(srv ArticleService) echo.HandlerFunc {
	return func(c echo.Context) (err error) {
		resp, err := srv.ListArticle(c)
		if err != nil {
			respFormat := &RespFormat{
				Status:  http.StatusInternalServerError,
				Message: err.Error(),
				Data:    resp,
			}
			return c.JSON(http.StatusInternalServerError, respFormat)
		}
		respFormat := &RespFormat{
			Status:  http.StatusOK,
			Message: "Success",
			Data:    resp,
		}
		return c.JSON(http.StatusOK, respFormat)
	}
}
