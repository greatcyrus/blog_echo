package main

import (
	"blog/internal/apisvr"
	"blog/pkg/article"
	"database/sql"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	_ "github.com/mattn/go-sqlite3"
	"github.com/volatiletech/sqlboiler/boil"
)

func main() {
	e := echo.New()
	e.Use(middleware.Logger())
	apisvr.RegisterArticleService(e, &article.ArticleService{})
	db, err := sql.Open("sqlite3", "db.sqlite")
	if err != nil {
		panic(err)
	}
	boil.SetDB(db)
	e.Logger.Fatal(e.Start(":8080"))
}
