package article

import (
	"blog/internal/apisvr"
	"blog/internal/models"
	"strconv"

	"github.com/labstack/echo"
	"github.com/volatiletech/sqlboiler/boil"
)

type ArticleService struct{}

func (s *ArticleService) CreateArticle(c echo.Context, req *apisvr.CreateArticleReq) (resp *apisvr.CreateArticleResp, err error) {
	article := models.Article{
		Title:   req.Title,
		Content: req.Content,
		Author:  req.Author,
	}
	err = article.InsertG(boil.Infer())
	if err != nil {
		return nil, err
	}
	resp = &apisvr.CreateArticleResp{
		ID: article.ID,
	}
	return resp, nil
}

func (s *ArticleService) RetrieveArticle(c echo.Context) (resp *apisvr.RetrieveArticleResp, err error) {
	id, _ := strconv.Atoi(c.Param("id"))
	article, err := models.FindArticleG(id)
	if err != nil {
		return nil, err
	}
	resp = &apisvr.RetrieveArticleResp{
		ID:      article.ID,
		Title:   article.Title,
		Content: article.Content,
		Author:  article.Author,
	}
	return resp, nil
}

func (s *ArticleService) ListArticle(c echo.Context) (resp []*apisvr.RetrieveArticleResp, err error) {
	resp = []*apisvr.RetrieveArticleResp{}
	articles, err := models.Articles().AllG()
	if err != nil {
		return nil, err
	}
	for _, article := range articles {
		resp = append(
			resp,
			&apisvr.RetrieveArticleResp{
				ID:      article.ID,
				Title:   article.Title,
				Content: article.Content,
				Author:  article.Author,
			},
		)
	}
	return resp, nil
}
