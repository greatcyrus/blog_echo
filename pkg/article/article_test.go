package article

import (
	"blog/internal/apisvr"
	"database/sql"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"os/exec"
	"strings"
	"testing"

	"github.com/labstack/echo"
	_ "github.com/mattn/go-sqlite3"
	"github.com/stretchr/testify/assert"
	"github.com/volatiletech/sqlboiler/boil"
)

var (
	mockData = `{"title":"Hello World","content":"Lorem ipsum dolor sit amet.","author":"Jack"}`
)

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	teardown()
	os.Exit(code)
}

func TestCreateArticle(t *testing.T) {
	db, _ := sql.Open("sqlite3", "test.sqlite")
	boil.SetDB(db)
	e := echo.New()
	s := &ArticleService{}
	req := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(mockData))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	resp := httptest.NewRecorder()
	c := e.NewContext(req, resp)
	handler := apisvr.CreateArticleHandler(s)
	if assert.NoError(t, handler(c)) {
		assert.Equal(t, http.StatusCreated, resp.Code)
	}
}

func TestRetrieveArticle(t *testing.T) {
	db, _ := sql.Open("sqlite3", "test.sqlite")
	boil.SetDB(db)
	e := echo.New()
	s := &ArticleService{}
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	resp := httptest.NewRecorder()
	c := e.NewContext(req, resp)
	c.SetPath("/articles/:id")
	c.SetParamNames("id")
	c.SetParamValues("1")
	handler := apisvr.RetrieveArticleHandler(s)
	if assert.NoError(t, handler(c)) {
		assert.Equal(t, http.StatusOK, resp.Code)
	}
}

func TestListArticle(t *testing.T) {
	db, _ := sql.Open("sqlite3", "test.sqlite")
	boil.SetDB(db)
	e := echo.New()
	s := &ArticleService{}
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	resp := httptest.NewRecorder()
	c := e.NewContext(req, resp)
	handler := apisvr.ListArticleHandler(s)
	if assert.NoError(t, handler(c)) {
		assert.Equal(t, http.StatusOK, resp.Code)
	}
}

func setup() {
	cmd := `cat ../../db/database.sql | sqlite3 test.sqlite`
	exec.Command("bash", "-c", cmd).Run()
	fmt.Printf("Setup completed\n")
}

func teardown() {
	cmd := `rm test.sqlite`
	exec.Command("bash", "-c", cmd).Run()
	fmt.Printf("Teardown completed\n")
}
